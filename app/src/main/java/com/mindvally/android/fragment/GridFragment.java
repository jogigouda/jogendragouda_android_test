//==============================================
//  Created By :    Jogendra Gouda
//  Created on :    13 JULY 2016
//==============================================
package com.mindvally.android.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mindvally.android.R;
import com.mindvally.android.activity.ImageDetailActivity;
import com.mindvally.android.adapter.FeedAdapter;
import com.mindvally.android.model.FeedItem;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import test.mindvally.com.mynetworklibrary.OnServiceCallListener;
import test.mindvally.com.mynetworklibrary.ServerResponse;
import test.mindvally.com.mynetworklibrary.cacheUtil.CacheWorker;
import test.mindvally.com.mynetworklibrary.logger.Logger;
import test.mindvally.com.mynetworklibrary.utils.InternetUtil;


/**
 * Placeholder fragment for {@link com.mindvally.android.activity.MainActivity}.
 */
public class GridFragment extends Fragment implements FeedAdapter.OnFeedItemClickListener, OnServiceCallListener {
    private RecyclerView rvFeedList;
    private Activity activity;
    private Logger logger;
    private FeedAdapter feedAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private CacheWorker cacheWorker;
    private final int PERMISSION_REQUEST_CODE = 121;

    public GridFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_main, container, false);
        activity = getActivity();
        cacheWorker = new CacheWorker(activity);
        cacheWorker.initWorker(getFragmentManager());
        bindViews(mainView);
        logger = new Logger();
        if (checkAndRequestPermissions()) {
            apiCall();
        }
        return mainView;
    }


    /**
     * Method to find all the views
     *
     * @param mainView parent view
     */
    private void bindViews(View mainView) {
        rvFeedList = (RecyclerView) mainView.findViewById(R.id.rvFeedList);
        mSwipeRefreshLayout = (SwipeRefreshLayout) mainView.findViewById(R.id.wrlFeedList);
        // Configure the refreshing colors
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                apiCall();
            }
        });
        setupRecycleView(rvFeedList);

    }

    /**
     * Method setup feeds recycleView and it's properties
     *
     * @param recyclerView recycleView to setUp
     */
    private void setupRecycleView(RecyclerView recyclerView) {
        recyclerView.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(activity, 2);
        layoutManager.setOrientation(GridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        List<FeedItem> feedItems = new ArrayList<>();
        feedAdapter = new FeedAdapter(activity, GridFragment.this, feedItems, cacheWorker);
        recyclerView.setAdapter(feedAdapter);
        rvFeedList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                // Pause fetcher to ensure smoother scrolling when flinging
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    cacheWorker.setPauseWork(true);
                } else if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    cacheWorker.setPauseWork(false);
                }
            }
        });
    }

    /**
     * Method to do server call
     */
    private void apiCall() {
        String Url = "http://pastebin.com/raw/wgkJgazE";
        try {
            if (InternetUtil.isOnline(activity)) {
                cacheWorker.getJson(Url, GridFragment.this);
            } else {
                Snackbar.make(rvFeedList, getResources().getString(R.string.noInternetMsg), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Check for marshmallow permission for internet
     */
    private boolean checkAndRequestPermissions() {
        int networkPermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_NETWORK_STATE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (networkPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_NETWORK_STATE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            this.requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSION_REQUEST_CODE);
            return false;
        }
        return true;
    }


    @Override
    public void onFeedItemClick(int position, View view) {
        final Intent i = new Intent(getActivity(), ImageDetailActivity.class);
        i.putExtra(ImageDetailActivity.IMAGE_DATA_EXTRA, feedAdapter.getItem(position).getLinks().get("full"));
        ActivityOptions options =
                ActivityOptions.makeScaleUpAnimation(view, 0, 0, view.getWidth(), view.getHeight());
        getActivity().startActivity(i, options.toBundle());
    }

    @Override
    public void onCallStart() {

    }

    @Override
    public void onCalling() {

    }

    @Override
    public void onResponse(ServerResponse serverResponse) {
        logger.setDebugMsg("Response", "" + serverResponse.getResponse());
        mSwipeRefreshLayout.setRefreshing(false);
        List<FeedItem> feedItems = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(serverResponse.getResponse());
            for (int i = 0; i < jsonArray.length(); i++) {
                feedItems.add(new FeedItem(jsonArray.get(i).toString()));
            }
            feedAdapter.setFeedItemArrayList(feedItems);
            feedAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        cacheWorker.setExitTasksEarly(false);
        feedAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        cacheWorker.setPauseWork(false);
        cacheWorker.setExitTasksEarly(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cacheWorker.closeCache();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            apiCall();
        }
    }


}
