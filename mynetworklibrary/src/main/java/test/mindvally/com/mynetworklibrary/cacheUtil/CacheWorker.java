//============================================
// Created by : Jogendra Gouda
// Created on : 13 July 2015
//=============================================

package test.mindvally.com.mynetworklibrary.cacheUtil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

import test.mindvally.com.mynetworklibrary.OnServiceCallListener;
import test.mindvally.com.mynetworklibrary.ServerResponse;

/**
 * Class to perform cache related operations
 * use {@code loadImage()} to load image to an image Asyncroniously and {@code getJson()} to get string in return
 */
public class CacheWorker {
    private static final String TAG = "CacheWorker";
    private static final int FADE_IN_TIME = 200;
    private LocalCache mImageCache;
    private Bitmap mLoadingBitmap;
    private boolean mFadeInBitmap = true;
    private boolean mExitTasksEarly = false;
    protected boolean mPauseWork = false;
    private final Object mPauseWorkLock = new Object();
    protected Resources mResources;
    private static final int MESSAGE_CLEAR = 0;
    private static final int MESSAGE_INIT_DISK_CACHE = 1;
    private static final int MESSAGE_CLOSE = 2;
    final String error_msg_Null_url = "URL is null";


    public CacheWorker(Context context) {
        mResources = context.getResources();

    }


    /**
     * Method to load image Asynchronously to image view
     *
     * @param data      Unique key used for storing in cache
     * @param imageView imageView to load image
     * @param listener  listener to notify image load
     */
    public void loadImage(Object data, ImageView imageView, OnImageLoadedListener listener) {
        if (data == null) {
            return;
        }
        Object value = null;

        if (mImageCache != null) {
            value = mImageCache.getObjectFromMemCache(String.valueOf(data));
        }

        if (value != null) {
            // found in memory cache
            BitmapDrawable drawable = (BitmapDrawable) value;
            imageView.setImageDrawable(drawable);
            if (listener != null) {
                listener.onImageLoaded(true);
            }
        } else if (cancelPotentialWork(data, imageView)) {
            //BEGIN_INCLUDE(execute_background_task)
            final BitmapWorkerTask task = new BitmapWorkerTask(data, imageView, listener);
            final AsyncDrawable asyncDrawable =
                    new AsyncDrawable(mResources, mLoadingBitmap, task);
            imageView.setImageDrawable(asyncDrawable);

            // NOTE: This uses a custom version of AsyncTask that has been pulled from the
            // framework and slightly modified. Refer to the docs at the top of the class
            // for more info on what was changed.
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    /**
     * Load an image specified by the data parameter into an ImageView (override
     * disk cache will be used if an {@link LocalCache} has been added using
     * image is found in the memory cache, it is set immediately, otherwise an {@link AsyncTask}
     * will be created to asynchronously load the bitmap.
     *
     * @param data      The URL of the image to download.
     * @param imageView The ImageView to bind the downloaded image to.
     */
    public void loadImage(Object data, ImageView imageView) {
        loadImage(data, imageView, null);
    }

    /**
     * Method to get json data from cache if not it will get json from api response and store in cache
     *
     * @param data         Unique key for getting json data I our case we have used URL
     * @param callListener listener to notify
     */

    public void getJson(Object data, OnServiceCallListener callListener) {
        ServerResponse response = new ServerResponse();
        if (data == null) {
            return;
        }
        Object value = null;
        if (mImageCache != null) {
            value = mImageCache.getObjectFromMemCache(String.valueOf(data));
        }
        if (value != null) {
            response.setResponse((String) value);
            callListener.onResponse(response);
        } else {
            AsyncServerCall asyncServerCall = new AsyncServerCall((String) data, callListener);
            asyncServerCall.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    /**
     * Set placeholder bitmap that shows when the the background thread is running.
     *
     * @param bitmap
     */
    public void setLoadingImage(Bitmap bitmap) {
        mLoadingBitmap = bitmap;
    }

    /**
     * Set placeholder bitmap that shows when the the background thread is running.
     *
     * @param resId
     */
    public void setLoadingImage(int resId) {
        mLoadingBitmap = BitmapFactory.decodeResource(mResources, resId);
    }

    /**
     * Adds an {@link LocalCache} to this {@link CacheWorker} to handle disk and memory object
     * caching.
     *
     * @param fragmentManager
     */
    public void initWorker(FragmentManager fragmentManager) {
        mImageCache = LocalCache.getInstance(fragmentManager);
        new CacheAsyncTask().execute(MESSAGE_INIT_DISK_CACHE);
    }


    public void setExitTasksEarly(boolean exitTasksEarly) {
        mExitTasksEarly = exitTasksEarly;
        setPauseWork(false);
    }


    /**
     * @return The {@link LocalCache} object currently being used by this CacheWorker.
     */
    protected LocalCache getImageCache() {
        return mImageCache;
    }

    public void setImageCache(LocalCache localCache) {
        this.mImageCache = localCache;
    }

    /**
     * Cancels any pending work attached to the provided ImageView.
     *
     * @param imageView
     */
    public static void cancelWork(ImageView imageView) {
        final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);
        if (bitmapWorkerTask != null) {
            bitmapWorkerTask.cancel(true);
        }
    }

    /**
     * Returns true if the current work has been canceled or if there was no work in
     * progress on this image view.
     * Returns false if the work in progress deals with the same data. The work is not
     * stopped in that case.
     */
    public static boolean cancelPotentialWork(Object data, ImageView imageView) {
        //BEGIN_INCLUDE(cancel_potential_work)
        final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

        if (bitmapWorkerTask != null) {
            final Object bitmapData = bitmapWorkerTask.mData;
            if (bitmapData == null || !bitmapData.equals(data)) {
                bitmapWorkerTask.cancel(true);
            } else {
                // The same work is already in progress.
                return false;
            }
        }
        return true;
    }

    /**
     * @param imageView Any imageView
     * @return Retrieve the currently active work task (if any) associated with this imageView.
     * null if there is no such task.
     */
    private static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
                return asyncDrawable.getBitmapWorkerTask();
            }
        }
        return null;
    }

    /**
     * The actual AsyncTask that will asynchronously process the image.
     */
    private class BitmapWorkerTask extends AsyncTask<Void, Void, BitmapDrawable> {
        private Object mData;
        private final WeakReference<ImageView> imageViewReference;
        private final OnImageLoadedListener mOnImageLoadedListener;

        public BitmapWorkerTask(Object data, ImageView imageView) {
            mData = data;
            imageViewReference = new WeakReference<ImageView>(imageView);
            mOnImageLoadedListener = null;
        }

        public BitmapWorkerTask(Object data, ImageView imageView, OnImageLoadedListener listener) {
            mData = data;
            imageViewReference = new WeakReference<ImageView>(imageView);
            mOnImageLoadedListener = listener;
        }

        /**
         * Background processing.
         */
        @Override
        protected BitmapDrawable doInBackground(Void... params) {
            final String dataString = String.valueOf(mData);
            Bitmap bitmap = null;
            BitmapDrawable drawable = null;

            // Wait here if work is paused and the task is not cancelled
            synchronized (mPauseWorkLock) {
                while (mPauseWork && !isCancelled()) {
                    try {
                        mPauseWorkLock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            // If the image cache is available and this task has not been cancelled by another
            // thread and the ImageView that was originally bound to this task is still bound back
            // to this task and our "exit early" flag is not set then try and fetch the bitmap from
            // the cache
            if (mImageCache != null && !isCancelled() && getAttachedImageView() != null
                    && !mExitTasksEarly) {
                BitmapDrawable drawable1 = (BitmapDrawable) mImageCache.getObjectFromMemCache(dataString);
                if (drawable1 != null)
                    bitmap = drawable1.getBitmap();
            }

            // If the bitmap was not found in the cache and this task has not been cancelled by
            // another thread and the ImageView that was originally bound to this task is still
            // bound back to this task and our "exit early" flag is not set, then call the main
            // process method (as implemented by a subclass)
            if (bitmap == null && !isCancelled() && getAttachedImageView() != null
                    && !mExitTasksEarly) {
                bitmap = new Utility().downloadUrlToStream(dataString);
                bitmap = new Utility().getResizedBitmap(bitmap, 300, 300);
            }

            // If the bitmap was processed and the image cache is available, then add the processed
            // bitmap to the cache for future use. Note we don't check if the task was cancelled
            // here, if it was, and the thread is still running, we may as well add the processed
            // bitmap to our cache as it might be used again in the future
            if (bitmap != null) {
                drawable = new BitmapDrawable(mResources, bitmap);
                if (mImageCache != null) {
                    mImageCache.addObjectToCache(dataString, drawable);
                }
            }
            return drawable;
        }

        /**
         * Once the image is processed, associates it to the imageView
         */
        @Override
        protected void onPostExecute(BitmapDrawable value) {
            //BEGIN_INCLUDE(complete_background_work)
            boolean success = false;
            // if cancel was called on this task or the "exit early" flag is set then we're done
            if (isCancelled() || mExitTasksEarly) {
                value = null;
            }

            final ImageView imageView = getAttachedImageView();
            if (value != null && imageView != null) {
                success = true;
                setImageDrawable(imageView, value);
            }
            if (mOnImageLoadedListener != null) {
                mOnImageLoadedListener.onImageLoaded(success);
            }
        }

        @Override
        protected void onCancelled(BitmapDrawable value) {
            super.onCancelled(value);
            synchronized (mPauseWorkLock) {
                mPauseWorkLock.notifyAll();
            }
        }

        /**
         * Returns the ImageView associated with this task as long as the ImageView's task still
         * points to this task as well. Returns null otherwise.
         */
        private ImageView getAttachedImageView() {
            final ImageView imageView = imageViewReference.get();
            final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

            if (this == bitmapWorkerTask) {
                return imageView;
            }

            return null;
        }
    }

    /**
     * Interface definition for callback on image loaded successfully.
     */
    public interface OnImageLoadedListener {

        /**
         * Called once the image has been loaded.
         *
         * @param success True if the image was loaded successfully, false if
         *                there was an error.
         */
        void onImageLoaded(boolean success);
    }

    /**
     * A custom Drawable that will be attached to the imageView while the work is in progress.
     * Contains a reference to the actual worker task, so that it can be stopped if a new binding is
     * required, and makes sure that only the last started worker process can bind its result,
     * independently of the finish order.
     */
    private static class AsyncDrawable extends BitmapDrawable {
        private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

        public AsyncDrawable(Resources res, Bitmap bitmap, BitmapWorkerTask bitmapWorkerTask) {
            super(res, bitmap);
            bitmapWorkerTaskReference =
                    new WeakReference<BitmapWorkerTask>(bitmapWorkerTask);
        }

        public BitmapWorkerTask getBitmapWorkerTask() {
            return bitmapWorkerTaskReference.get();
        }
    }

    /**
     * Called when the processing is complete and the final drawable should be
     * set on the ImageView.
     *
     * @param imageView
     * @param drawable
     */
    private void setImageDrawable(ImageView imageView, Drawable drawable) {
        if (mFadeInBitmap) {
            // Transition drawable with a transparent drawable and the final drawable
            final TransitionDrawable td =
                    new TransitionDrawable(new Drawable[]{
                            new ColorDrawable(Color.parseColor("#00000000")),
                            drawable
                    });
            // Set background to loading bitmap
            imageView.setBackground(
                    new BitmapDrawable(mResources, mLoadingBitmap));

            imageView.setImageDrawable(td);
            td.startTransition(FADE_IN_TIME);
        } else {
            imageView.setImageDrawable(drawable);
        }
    }

    /**
     * Pause any ongoing background work. This can be used as a temporary
     * measure to improve performance. For example background work could
     * be paused when a ListView or GridView is being scrolled using a
     * {@link android.widget.AbsListView.OnScrollListener} to keep
     * scrolling smooth.
     * <p/>
     * If work is paused, be sure setPauseWork(false) is called again
     * before your fragment or activity is destroyed (for example during
     * {@link android.app.Activity#onPause()}), or there is a risk the
     * background thread will never finish.
     */
    public void setPauseWork(boolean pauseWork) {
        synchronized (mPauseWorkLock) {
            mPauseWork = pauseWork;
            if (!mPauseWork) {
                mPauseWorkLock.notifyAll();
            }
        }
    }

    protected class CacheAsyncTask extends AsyncTask<Object, Void, Void> {

        @Override
        protected Void doInBackground(Object... params) {
            switch ((Integer) params[0]) {
                case MESSAGE_CLEAR:
                    clearCacheInternal();
                    break;
                case MESSAGE_INIT_DISK_CACHE:
                    initDiskCacheInternal();
                    break;
                case MESSAGE_CLOSE:
                    closeCacheInternal();
                    break;
            }
            return null;
        }
    }

    public void initDiskCacheInternal() {
        if (mImageCache != null) {
            mImageCache.init();
        }
    }


    protected void clearCacheInternal() {
        if (mImageCache != null) {
            mImageCache.clearCache();
        }
    }

    protected void closeCacheInternal() {
        if (mImageCache != null) {
            mImageCache = null;
        }
    }

    public void clearCache() {
        new CacheAsyncTask().execute(MESSAGE_CLEAR);
    }

    public void closeCache() {
        new CacheAsyncTask().execute(MESSAGE_CLOSE);
    }


    /**
     * Async class for server call
     */
    private class AsyncServerCall extends AsyncTask<Void, Void, String> {
        private String url;
        private OnServiceCallListener callListener;
        private ServerResponse serverResponse = new ServerResponse();

        public AsyncServerCall(String url, OnServiceCallListener callListener) {
            this.url = url;
            this.callListener = callListener;
            this.serverResponse = new ServerResponse();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            callListener.onCallStart();
        }

        @Override
        protected String doInBackground(Void... param) {
            String response = null;
            HttpURLConnection urlConn = null;
            try {
                callListener.onCalling();
                // Wait here if work is paused and the task is not cancelled
                synchronized (mPauseWorkLock) {
                    while (mPauseWork && !isCancelled()) {
                        try {
                            mPauseWorkLock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (mImageCache != null && !isCancelled()
                        && !mExitTasksEarly) {
                    response = (String) mImageCache.getObjectFromMemCache(url);
                }

                if (response == null && !isCancelled()
                        && !mExitTasksEarly) {
                    int readTimeOut = 10000;
                    int connectionTimeOut = 15000;
                    if (url != null) {
                        URL Url = new URL(url);
                        urlConn = (HttpURLConnection) Url.openConnection();
                        urlConn.setReadTimeout(readTimeOut);
                        urlConn.setConnectTimeout(connectionTimeOut);
                        urlConn.setRequestMethod("GET");   // By Default
                        InputStream in = new BufferedInputStream(urlConn.getInputStream());
                        response = new Utility().readStream(in);
                        if (urlConn != null) {
                            serverResponse.setResponseCode("" + urlConn.getResponseCode());
                            serverResponse.setResponseMsg(urlConn.getResponseMessage());
                            serverResponse.setResponse(response);
                            if (mImageCache != null) {
                                mImageCache.addObjectToCache(url, response);
                            }
                        }
                    } else {
                        throw new Exception(error_msg_Null_url);
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (urlConn != null) {
                    urlConn.disconnect();
                }
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            try {
                callListener.onResponse(serverResponse);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            synchronized (mPauseWorkLock) {
                mPauseWorkLock.notifyAll();
            }
        }
    }
}
