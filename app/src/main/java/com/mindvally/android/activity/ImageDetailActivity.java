//============================================
// Created by : Jogendra Gouda
// Created on : 15 JULY 2016
//=============================================

package com.mindvally.android.activity;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.mindvally.android.R;

import test.mindvally.com.mynetworklibrary.cacheUtil.CacheWorker;
import test.mindvally.com.mynetworklibrary.utils.InternetUtil;

/**
 * Image detail activity for showing large image of feed
 */
public class ImageDetailActivity extends AppCompatActivity {
    public static final String IMAGE_DATA_EXTRA = "extra_image_data";
    private ImageView mImageView;
    private CacheWorker cacheWorker;

    /**
     * Empty constructor as per the Fragment documentation
     */
    public ImageDetailActivity() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String mImageUrl = getIntent().hasExtra(IMAGE_DATA_EXTRA) ? getIntent().getStringExtra(IMAGE_DATA_EXTRA) : null;
        setContentView(R.layout.image_detail_fragment);
        mImageView = (ImageView) findViewById(R.id.imageView);
        cacheWorker = new CacheWorker(this);
        cacheWorker.initDiskCacheInternal();
        if (InternetUtil.isOnline(this)) {
            cacheWorker.loadImage(mImageUrl, mImageView);
        } else {
            Snackbar.make(mImageView, getResources().getString(R.string.noInternetMsg), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mImageView != null) {
            // Cancel any pending image work
            cacheWorker.cancelWork(mImageView);
            mImageView.setImageDrawable(null);
        }
    }
}
