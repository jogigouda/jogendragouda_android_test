//==============================================
//  Created By :    Jogendra Gouda
//  Created on :    13 July 2016
//==============================================
package com.mindvally.android.model;

/**
 * Model class to hold properties of feed user
 */

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Model class holds user detail information
 */
public class User {
    private String id, username, name;
    private HashMap<String, String> profileImageHashMap;
    private HashMap<String, String> links;

    public User() {
    }

    public User(String id, HashMap<String, String> links, String name, HashMap<String, String> profileImageHashMap, String username) {
        this.id = id;
        this.links = links;
        this.name = name;
        this.profileImageHashMap = profileImageHashMap;
        this.username = username;
    }

    /**
     * Constructor to create User from json String
     *
     * @param jsonString User Json String
     * @throws JSONException
     */
    public User(String jsonString) throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonString);
        this.id = jsonObject.optString("id");
        this.username = jsonObject.optString("username");
        this.name = jsonObject.optString("name");
        if (jsonObject.has("profile_image")) {
            JSONObject profileImageJson = jsonObject.getJSONObject("profile_image");
            this.profileImageHashMap = new HashMap<>();
            profileImageHashMap.put("small", profileImageJson.optString("small"));
            profileImageHashMap.put("medium", profileImageJson.optString("medium"));
            profileImageHashMap.put("large", profileImageJson.optString("large"));
        }

        if (jsonObject.has("links")) {
            JSONObject linkJson = jsonObject.getJSONObject("links");
            this.links = new HashMap<>();
            links.put("self", linkJson.optString("self"));
            links.put("html", linkJson.optString("html"));
            links.put("photos", linkJson.optString("photos"));
            links.put("likes", linkJson.optString("likes"));
        }

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public HashMap<String, String> getLinks() {
        return links;
    }

    public void setLinks(HashMap<String, String> links) {
        this.links = links;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<String, String> getProfileImageHashMap() {
        return profileImageHashMap;
    }

    public void setProfileImageHashMap(HashMap<String, String> profileImageHashMap) {
        this.profileImageHashMap = profileImageHashMap;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
