//============================================================
//   Created By:   Jogendra Gouda
//   Created on:   13 JULY 2016
//  ==========================================================

package test.mindvally.com.mynetworklibrary.logger;

import android.util.Log;

/**
 * Class to handle log Messages through out the application
 * there will be to Mods of an application Development Mode and Release Mode
 * if Mode is set to Development Allow app to show Logs in Logcat
 * and if Mode is set to Release do not allow app To show Logs
 */
public class Logger {


    /**
     * Enum to define App mode
     * 0 Development
     * 1 Release
     */
    public enum ApplicationMode {
        Development,
        Release,
    }

    /**
     * Variable Store Current App mode
     * Change the mode to Release while relesing application to
     */
    ApplicationMode appmode = ApplicationMode.Development;


    /**
     * Method to Set debug Message
     *
     * @param name  title of message
     * @param value Value of message
     */
    public void setDebugMsg(String name, String value) {

        if (appmode == ApplicationMode.Development)
            Log.d(name, value);

    }

    /**
     * Method to Set Error Message
     *
     * @param name  title of message
     * @param value Value of message
     */
    public void setErrorMsg(String name, String value) {
        if (appmode == ApplicationMode.Development)
            Log.e(name, value);

    }


    /**
     * Method to Set Information Message
     *
     * @param name  title of message
     * @param value Value of message
     */
    public void setInfoMsg(String name, String value) {
        if (appmode == ApplicationMode.Development)
            Log.i(name, value);
    }

}
