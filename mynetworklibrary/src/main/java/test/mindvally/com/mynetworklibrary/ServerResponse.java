//============================================
// Created by : Jogendra Gouda
// Created on : 12 JULY 2016
//=============================================
package test.mindvally.com.mynetworklibrary;

/**
 * Model to to hold detail of response
 */
public class ServerResponse {
    private String ResponseCode, ResponseMsg, Response, ErrorMessage;

    public ServerResponse() {
    }

    public ServerResponse(String responseCode, String response, String errorMessage) {
        ResponseCode = responseCode;
        Response = response;
        ErrorMessage = errorMessage;
    }

    public String getResponseMsg() {
        return ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public String getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(String responseCode) {
        ResponseCode = responseCode;
    }

    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        Response = response;
    }

    public String getErrorMessage() {
        return ErrorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        ErrorMessage = errorMessage;
    }


}
