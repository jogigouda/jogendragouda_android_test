//============================================
// Created by : Jogendra Gouda
// Created on : 12 JULy 2015
//=============================================

package test.mindvally.com.mynetworklibrary;

/**
 * interface to get Web service Events
 */
public interface OnServiceCallListener {
    void onCallStart();   //Service call Stars

    void onCalling();  //Service call going on

    void onResponse(ServerResponse serverResponse);   //Getting response from Server
}
