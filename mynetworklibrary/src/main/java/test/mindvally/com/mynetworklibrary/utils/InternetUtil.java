//============================================
// Created by : Jogendra Gouda
// Created on : 12 JULY 2015
//=============================================

package test.mindvally.com.mynetworklibrary.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Utility class for internet related operations
 */
public class InternetUtil {

    /**
     * Method to return that device is connected to internet or not
     *
     * @param context
     */
    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
