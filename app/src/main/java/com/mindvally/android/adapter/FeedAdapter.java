//==============================================
//  Created By :    Jogendra Gouda
//  Created on :    13 JULY 2016
//==============================================
package com.mindvally.android.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindvally.android.R;
import com.mindvally.android.model.FeedItem;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import test.mindvally.com.mynetworklibrary.cacheUtil.CacheWorker;

/**
 * Adapter class for feed recycleView used in {@link com.mindvally.android.fragment.GridFragment}
 */
public class FeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private OnFeedItemClickListener listener;
    private List<FeedItem> feedItemArrayList;
    private CacheWorker cacheWorker;


    public FeedAdapter(Context context, OnFeedItemClickListener feedItemClickListener, List<FeedItem> feedItemArrayList, CacheWorker cacheWorker) {
        this.context = context;
        this.listener = feedItemClickListener;
        this.feedItemArrayList = feedItemArrayList;
        this.cacheWorker = cacheWorker;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.feed_grid_single_item, parent, false);
        viewHolder = new RecyclerViewHolders(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        FeedItem model = feedItemArrayList.get(holder.getAdapterPosition());
        RecyclerViewHolders viewHolder = (RecyclerViewHolders) holder;
        viewHolder.mTvLikes.setText(context.getResources().getString(R.string.labelLikes) + model.getLikes());
        viewHolder.mTvCategories.setText(model.getCategories().get(0).getTitle());
        viewHolder.mTvUserName.setText(model.getUser().getName());
        String isLiked = model.getLiked_by_user() ? context.getResources().getString(R.string.labelYes) : context.getResources().getString(R.string.labelNo);
        viewHolder.tvIsLikeByUser.setText(context.getResources().getString(R.string.likedByUser) + isLiked);

        //Feed image click handling
        viewHolder.mImgv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onFeedItemClick(position,view);
            }
        });

        try {
            //Loading feed image
            cacheWorker.loadImage(new URL(model.getUser().getProfileImageHashMap().get("large")), viewHolder.mImgvUserProfile);
            //Loading user profile thumbnail
            cacheWorker.loadImage(new URL(model.getLinks().get("raw")), viewHolder.mImgv);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return feedItemArrayList.size();
    }

    /**
     * Method to return feedItem of a particular position
     */
    public FeedItem getItem(int position) {
        return feedItemArrayList.get(position);
    }

    /**
     * Method to set adapter feed list
     */
    public void setFeedItemArrayList(List<FeedItem> feedItemArrayList) {
        this.feedItemArrayList = feedItemArrayList;
    }


    /**
     * Holder class of feed items
     */
    public class RecyclerViewHolders extends RecyclerView.ViewHolder {
        private ImageView mImgv, mImgvUserProfile;
        private TextView mTvUserName, mTvLikes, mTvCategories, tvIsLikeByUser;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            mImgv = (ImageView) itemView.findViewById(R.id.imgv);
            mImgvUserProfile = (ImageView) itemView.findViewById(R.id.imgvUserProfile);
            mTvLikes = (TextView) itemView.findViewById(R.id.tvLikes);
            mTvCategories = (TextView) itemView.findViewById(R.id.tvCategories);
            mTvUserName = (TextView) itemView.findViewById(R.id.tvUserName);
            tvIsLikeByUser = (TextView) itemView.findViewById(R.id.tvIsLikeByUser);
        }
    }

    /**
     * Interface definition for callback on feed item image.
     */
    public interface OnFeedItemClickListener {

        /**
         * Called when click performed on feed image.
         *
         * @param position position of feed item on which click performed.
         * @param view     view on which click performed
         */
        void onFeedItemClick(int position, View view);
    }
}
