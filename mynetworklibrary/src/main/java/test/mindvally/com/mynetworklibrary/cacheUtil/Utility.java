//============================================
// Created by : Jogendra Gouda
// Created on : 15 JULY 2016
//=============================================
package test.mindvally.com.mynetworklibrary.cacheUtil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Utility class holding methods used in caching
 */
public class Utility {
    private static final int IO_BUFFER_SIZE = 8 * 1024;
    private static final String TAG = "Utility";

    /**
     * Convert InputStream to String
     *
     * @param in inputStream for reading response
     */
    public String readStream(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    /**
     * Download a bitmap from a URL.
     *
     * @param urlString The URL to fetch
     * @return bitmap downloaded bitmap
     */
    public Bitmap downloadUrlToStream(String urlString) {
        HttpURLConnection urlConnection = null;
        BufferedInputStream in = null;

        try {
            final URL url = new URL(urlString);
            urlConnection = (HttpURLConnection) url.openConnection();
            in = new BufferedInputStream(urlConnection.getInputStream(), IO_BUFFER_SIZE);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            return BitmapFactory.decodeStream(in, null, options);
        } catch (final IOException e) {
            Log.e(TAG, "Error in downloadBitmap - " + e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            try {
                if (in != null) {
                    in.close();
                }
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Method to resize large size images
     *
     * @param bitmap    to resize
     * @param newHeight resize height
     * @param newWidth  resize width
     */
    public Bitmap getResizedBitmap(Bitmap bitmap, int newHeight, int newWidth) {
        Bitmap resizedBitmap = null;
        if (bitmap != null) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            float scaleWidth = ((float) newWidth) / width;
            float scaleHeight = ((float) newHeight) / height;
            // CREATE A MATRIX FOR THE MANIPULATION
            Matrix matrix = new Matrix();
            // RESIZE THE BIT MAP
            matrix.postScale(scaleWidth, scaleHeight);

            // "RECREATE" THE NEW BITMAP
            resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height,
                    matrix, false);
        }
        return resizedBitmap;
    }

}
