//==============================================
//  Created By :    Jogendra Gouda
//  Created on :    13 JULY 2016
//==============================================
package com.mindvally.android.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Model to store feed categories
 */
public class Categories {
    private String id, title;
    private int photo_count;
    private HashMap<String, String> links;

    public Categories() {
    }

    public Categories(String id, HashMap<String, String> links, int photo_count, String title) {
        this.id = id;
        this.links = links;
        this.photo_count = photo_count;
        this.title = title;
    }

    /**
     * Constructor to create Category from json String
     *
     * @param jsonString Category Json
     * @throws JSONException
     */
    public Categories(String jsonString) throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonString);
        this.id = jsonObject.optString("id");
        this.title = jsonObject.optString("title");
        this.photo_count = jsonObject.optInt("photo_count");
        if (jsonObject.has("links")) {
            JSONObject linksJson = jsonObject.getJSONObject("links");
            this.links = new HashMap<>();
            links.put("self", linksJson.optString("self"));
            links.put("photos", linksJson.optString("photos"));
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public HashMap<String, String> getLinks() {
        return links;
    }

    public void setLinks(HashMap<String, String> links) {
        this.links = links;
    }

    public int getPhoto_count() {
        return photo_count;
    }

    public void setPhoto_count(int photo_count) {
        this.photo_count = photo_count;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
