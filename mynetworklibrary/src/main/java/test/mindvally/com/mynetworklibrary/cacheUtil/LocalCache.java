//============================================
// Created by : Jogendra Gouda
// Created on : 13 JULY 2016
//=============================================

package test.mindvally.com.mynetworklibrary.cacheUtil;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.LruCache;

import test.mindvally.com.mynetworklibrary.logger.Logger;

/**
 * Class to manage local cache operations.
 */
public class LocalCache {
    private static final String TAG = "LocalCache";

    // Default memory cache size in kilobytes
    private static final int DEFAULT_MEM_CACHE_SIZE = 1024 * 2; // 2MB

    // Constants to easily toggle various caches
    private static final boolean DEFAULT_MEM_CACHE_ENABLED = true;
    private LruCache<String, Object> mMemoryCache;
    private final Object mPauseWorkLock = new Object();
    private Logger logger = new Logger();

    private LocalCache() {
        init();
    }

    /**
     * Return an {@link LocalCache} instance. A {@link RetainFragment} is used to retain the
     * LocalCache object across configuration changes such as a change in device orientation.
     *
     * @param fragmentManager The fragment manager to use when dealing with the retained fragment.
     * @return An existing retained LocalCache object or a new one if one did not exist
     */
    public static LocalCache getInstance(
            FragmentManager fragmentManager) {

        // Search for, or create an instance of the non-UI RetainFragment
        final RetainFragment mRetainFragment = findOrCreateRetainFragment(fragmentManager);

        // See if we already have an LocalCache stored in RetainFragment
        LocalCache imageCache = (LocalCache) mRetainFragment.getObject();

        // No existing LocalCache, create one and store it in RetainFragment
        if (imageCache == null) {
            imageCache = new LocalCache();
            mRetainFragment.setObject(imageCache);
        }

        return imageCache;
    }

    /**
     * Initialize the cache, providing all parameters.
     */
    public void init() {
        synchronized (mPauseWorkLock) {
            // Set up memory cache
            if (DEFAULT_MEM_CACHE_ENABLED) {
                mMemoryCache = new LruCache<String, Object>(DEFAULT_MEM_CACHE_SIZE) {
                    /**
                     * Measure item size in kilobytes rather than units which is more practical
                     * for a bitmap cache
                     */
                    @Override
                    protected int sizeOf(String key, Object value) {
                        final int bitmapSize = getSize(value) / 1024;
                        return bitmapSize == 0 ? 1 : bitmapSize;
                    }
                };
            }
        }
    }


    /**
     * Adds a Object to both memory and disk cache.
     *
     * @param data  Unique identifier for the bitmap to store
     * @param value The bitmap drawable to store
     */
    public void addObjectToCache(String data, Object value) {
        synchronized (mPauseWorkLock) {
            if (data == null || value == null) {
                return;
            }
            // Add to memory cache
            if (mMemoryCache != null) {
                mMemoryCache.put(data, value);
                logger.setInfoMsg(TAG, "Object added to cache");
            }
        }
    }

    /**
     * Get from memory cache.
     *
     * @param data Unique identifier for which item to get
     * @return The object if found in cache, null otherwise
     */
    public Object getObjectFromMemCache(String data) {
        synchronized (mPauseWorkLock) {
            Object memValue = null;
            if (mMemoryCache != null) {
                memValue = mMemoryCache.get(data);
                logger.setInfoMsg(TAG, "Object fetched from cache");
            }
            return memValue;
        }
    }


    /**
     * Clears cache associated with this LocalCache object. Note that
     * this includes disk access so this should not be executed on the main/UI thread.
     */
    public void clearCache() {
        if (mMemoryCache != null) {
            mMemoryCache.evictAll();
            logger.setInfoMsg(TAG, "Cache cleared");
        }

    }

    /**
     * Get the size in bytes of an Object
     *
     * @param value Object to get size in bytes
     * @return size in bytes
     */
    public static int getSize(Object value) {
        int bytes = 0;
        if (value instanceof String) {
            bytes = ((String) value).getBytes().length;
        } else if (value instanceof BitmapDrawable) {
            bytes = ((BitmapDrawable) value).getBitmap().getByteCount();
        }
        return bytes;
    }

    /**
     * Locate an existing instance of this Fragment or if not found, create and
     * add it using FragmentManager.
     *
     * @param fm The FragmentManager manager to use.
     * @return The existing instance of the Fragment or the new instance if just
     * created.
     */
    private static RetainFragment findOrCreateRetainFragment(FragmentManager fm) {
        //BEGIN_INCLUDE(find_create_retain_fragment)
        // Check to see if we have retained the worker fragment.
        RetainFragment mRetainFragment = (RetainFragment) fm.findFragmentByTag(TAG);

        // If not retained (or first time running), we need to create and add it.
        if (mRetainFragment == null) {
            mRetainFragment = new RetainFragment();
            fm.beginTransaction().add(mRetainFragment, TAG).commitAllowingStateLoss();
        }

        return mRetainFragment;
        //END_INCLUDE(find_create_retain_fragment)
    }

    /**
     * A simple non-UI Fragment that stores a single Object and is retained over configuration
     * changes. It will be used to retain the LocalCache object.
     */
    public static class RetainFragment extends Fragment {
        private Object mObject;

        /**
         * Empty constructor as per the Fragment documentation
         */
        public RetainFragment() {
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Make sure this Fragment is retained over a configuration change
            setRetainInstance(true);
        }

        /**
         * Store a single object in this Fragment.
         *
         * @param object The object to store
         */
        public void setObject(Object object) {
            mObject = object;
        }

        /**
         * Get the stored object.
         *
         * @return The stored object
         */
        public Object getObject() {
            return mObject;
        }
    }
}
