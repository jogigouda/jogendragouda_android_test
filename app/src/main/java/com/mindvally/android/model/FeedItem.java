//==============================================
//  Created By :    Jogendra Gouda
//  Created on :    13 July 2016
//==============================================
package com.mindvally.android.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Model class to hold properties of feed
 */
public class FeedItem {
    private String id;
    private String created_at, color;
    private int width, height;
    private User user;
    private ArrayList<Categories> categories;
    private HashMap<String, String> links;
    private HashMap<String, String> urls;
    private int likes;
    private boolean liked_by_user;

    public FeedItem() {

    }

    /**
     * Constructor to create FeedItem from json String
     *
     * @param jsonString Feed Json
     * @throws JSONException
     */
    public FeedItem(String jsonString) throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonString);
        this.id = jsonObject.optString("id");
        this.created_at = jsonObject.optString("created_at");
        this.width = jsonObject.optInt("width");
        this.height = jsonObject.optInt("height");
        this.color = jsonObject.optString("color");
        this.likes = jsonObject.optInt("likes");
        this.liked_by_user = jsonObject.optBoolean("liked_by_user");
        if (jsonObject.has("user")) {
            user = new User(jsonObject.optString("user"));
        }
        if (jsonObject.has("categories")) {
            JSONArray jsonArray = jsonObject.optJSONArray("categories");
            this.categories = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                categories.add(new Categories(jsonArray.getJSONObject(0).toString()));
            }
        }
        if (jsonObject.has("links")) {
            JSONObject linksJson = jsonObject.getJSONObject("links");
            this.links = new HashMap<>();
            links.put("self", linksJson.optString("self"));
            links.put("html", linksJson.optString("html"));
            links.put("download", linksJson.optString("download"));
        }

        if (jsonObject.has("urls")) {
            JSONObject linksJson = jsonObject.getJSONObject("urls");
            this.urls = new HashMap<>();
            links.put("raw", linksJson.optString("raw"));
            links.put("full", linksJson.optString("full"));
            links.put("regular", linksJson.optString("regular"));
            links.put("small", linksJson.optString("small"));
            links.put("thumb", linksJson.optString("thumb"));
        }

    }

    public ArrayList<Categories> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Categories> categories) {
        this.categories = categories;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean getLiked_by_user() {
        return liked_by_user;
    }

    public void setLiked_by_user(boolean liked_by_user) {
        this.liked_by_user = liked_by_user;
    }

    public HashMap<String, String> getLinks() {
        return links;
    }

    public void setLinks(HashMap<String, String> links) {
        this.links = links;
    }

    public HashMap<String, String> getUrls() {
        return urls;
    }

    public void setUrls(HashMap<String, String> urls) {
        this.urls = urls;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public boolean isLiked_by_user() {
        return liked_by_user;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }
}
